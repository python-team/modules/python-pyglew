
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////

#include "storagesize.hh"
#include "shape.hh"
#include "unpack_ptr.hh"

PyObject* __glReadPixels(PyObject*, PyObject* args)
{
  GLint x, y;
  GLsizei w, h;
  GLenum format, type;

  if ( PyTuple_Size(args) == 6 ) { 
    // reading back to client memory
    if ( !PyArg_ParseTuple(args, "iiiiII", &x, &y, &w, &h, &format, &type) ) {
      return NULL;
    }

    size_t size = w*h*shape(format)*storagesize(type);
    PyObject* buf = PyBuffer_New(size);
    
    GLvoid* ptr;
    int len;
    
    if ( PyObject_AsWriteBuffer(buf, &ptr, &len) < 0 || len != size ) { 
      PyErr_SetString(PyExc_RuntimeError, "Internal error in glReadPixels. Could not convert last arg to write buffer");
      return NULL;
    }
    
    glReadPixels(x, y, w, h, format, type, ptr);

    return buf;  
  } else if ( PyTuple_Size(args) == 7 ) { 
    // reading back to user-specified object
    PyObject* ptrobj;

    if ( !PyArg_ParseTuple(args, "iiiiIIO", &x, &y, &w, &h, &format, &type, &ptrobj) ) {
      return NULL;
    }

    if ( PyInt_Check(ptrobj) ) {
      // Access as PBO/VBO
      GLsizeiptrARB offset = PyInt_AsLong(ptrobj);
      glReadPixels(x, y, w, h, format, type, (char*)NULL + offset);
      Py_INCREF(Py_None);
      return Py_None; 

    } else if ( ptrobj == Py_None ) {
      // Access as PBO/VBO (or error)
      // should really check if PBOs are bound here to avoid fatal crash...
      // !!FIXME!!
      glReadPixels(x, y, w, h, format, type, NULL);
      Py_INCREF(Py_None);
      return Py_None;

    } else {
      void* ptr;
      int len;
      if ( PyObject_AsWriteBuffer(ptrobj, &ptr, &len) < 0) {
        PyErr_SetString(PyExc_RuntimeError, "Internal error in glReadPixels. Could not convert last arg to write buffer");
        return NULL;
      }

      size_t size = w*h*shape(format)*storagesize(type);
      if ( len > size ) {
        PyErr_SetString(PyExc_RuntimeError, "Internal error in glReadPixels. Write buffer too small");
        return NULL;
      }

      glReadPixels(x, y, w, h, format, type, ptr);

      return ptrobj;
    }
  } else {
    PyErr_SetString(PyExc_RuntimeError, "Wrong number of arguments to glReadPixels" );
    return NULL;
  }
}

