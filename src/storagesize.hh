
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////

#include "pyglew_exception.hh"

#ifndef __storagesize_hh__
#define __storagesize_hh__

size_t storagesize( GLenum type ) {
  switch ( type ) {
  case GL_UNSIGNED_BYTE:
    return sizeof(GLubyte);
  case GL_BYTE:
    return sizeof(GLbyte);
  case GL_UNSIGNED_SHORT:
    return sizeof(GLushort);
  case GL_SHORT:
    return sizeof(GLushort);
  case GL_UNSIGNED_INT:
    return sizeof(GLuint);
  case GL_INT:
    return sizeof(GLint);
  case GL_FLOAT:
    return sizeof(GLfloat);
  default:
    throw pyglew_exception("Error when determining storagesize for %d", type);
  };
}

#endif /* __storagesize_hh__ */
