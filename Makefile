builddir=build

GLBASE = $(subst extensions/,,$(wildcard extensions/GL_*))
GLOBJECTS = $(addprefix $(builddir)/, $(addsuffix -gen.o, $(GLBASE)))
GLCCS = $(addprefix $(builddir)/, $(addsuffix -gen.cc, $(GLBASE)))
GLHHS = $(addprefix include/, pack.hh unpack.hh unpack_ptr.hh pyglew_exception.hh pointer_wrapper.hh)

PYTHON=python2.4

all:

include $(shell uname -s).conf

all: exts pyglew.$(SOEXT)

test: pyglew.$(SOEXT)
	$(PYTHON) test.py

install: pyglew.$(SOEXT)
	$(PYTHON) setup.py install

pyglew.$(SOEXT): pyglew.o $(GLOBJECTS)
	$(LINK) -o $@ $^ $(LIBS)

$(builddir)/pyglew-gen.hh: $(addprefix $(builddir)/, $(addsuffix -gen.hh, $(GLBASE)))
	cat $^ > $@

$(builddir)/pyglew-methods.cc: $(addprefix $(builddir)/, $(addsuffix -methods.cc, $(GLBASE)))
	cat $^ > $@

$(builddir)/pyglew-constants.cc: $(addprefix $(builddir)/, $(addsuffix -constants.cc, $(GLBASE)))
	rm -f $@ ; 
	for file in $^; do echo "/* " $$file " */" >> $@; cat $$file >> $@; done;

pyglew.o: pyglew.cc $(GLHHS) $(builddir)/pyglew-gen.hh $(builddir)/pyglew-methods.cc $(builddir)/pyglew-constants.cc
	$(COMPILE) $(INCLUDES) -Iinclude -I$(builddir) -c $< -o $@

$(builddir)/%-gen.o: $(builddir)/%-gen.cc $(GLHHS)
	$(COMPILE) $(INCLUDES)  -c $< -o $@

$(builddir)/%-gen.hh $(builddir)/%-gen.cc $(builddir)/%-constants.cc: extensions/% main.py src/*.cc
	mkdir -p $(builddir)
	$(PYTHON) main.py $(builddir) $<

exts: 
	for ext in $(GLBASE) ; do make $(builddir)/$$ext-constants.cc; done


test_pbo: test_pbo.cc
	$(CXX) $(CXXFLAGS) $^ -o $@ -lGLEW -lGL -lSDL

clean:
	rm -f pyglew.so test_pbo
	rm -rf $(builddir)
	rm -rf dist

.PRECIOUS: $(builddir)/%-gen.hh $(builddir)/%-gen.cc $(builddir)/%-constants.cc $(builddir)/%-gen.o
.PHONY: exts dist
