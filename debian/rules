#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpatch/dpatch.make
include /usr/share/python/python.mk

PYVERS=$(shell pyversions -vr)

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
  CXXFLAGS += -O0
else
  CXXFLAGS += -O2
endif
export CXXFLAGS

clean: unpatch
	dh_testdir
	dh_testroot
	[ ! -d build ] || rm -rf build
	[ ! -d dist ]  || rm -rf dist
	dh_clean *.o *.so build-ext-* build-stamp debian/shlibs.local

build-ext-%:
	dh_testdir
	[ ! -d build ] || rm -rf build
	mkdir -p debian/python-pyglew/usr/lib/python$*/$(call py_sitename, $*)
	
	$(MAKE) PYTHON=python$*
	dh_install build/pyglew.so /usr/lib/python$*/$(call py_sitename, $*)
	
	# workaround to let think libpython<VER> is local, so that dh_shlibdebs does not generate
	# dependencies on them, but only ${python:Depends} will
	echo libpython$* 1.0 >> debian/shlibs.local
	touch $@

build: build-stamp
build-stamp: patch-stamp $(PYVERS:%=build-ext-%)
	dh_testdir
	touch $@

install: build
	dh_testdir
	dh_testroot
	dh_installdirs
	python setup.py install --root=$(CURDIR)/debian/python-pyglew $(py_setup_install_args)

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs
	dh_installdocs README
	dh_pysupport
	dh_installman
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
