python-pyglew (0.1.2-5) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Fix a typo in the package description.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jakub Wilk <jwilk@debian.org>  Fri, 21 Jun 2013 19:55:27 +0200

python-pyglew (0.1.2-4) unstable; urgency=low

  [ Julien Lavergne ]
  * From Ubuntu, prepare for the future python transition,
    thanks to Josselin Mouette for the report and to Alessio Treglia for the
    repot and patch; Closes: #521679, #547849
   - debian/rules :
    + Include /usr/share/python/python.mk
    + Append $(py_setup_install_args) macro to setup.py options to install
      all python stuffs in the right places.
    + Use $(call py_sitename, $*) macro to install the modules into the
      appropriate path (/usr/share/python*/*-packages).
   - debian/control :
    + Build-depends on python and python-all-dev (>= 2.5.4-1~)
    + Build-depends on debhelper (>= 5.0.38)

  [ Sandro Tosi ]
  * debian/control
    - bump Standards-Version to 3.8.3 (no changes needed)
  * debian/{contro, rules, python-pyglew.preinst}
    - migrated from python-central to python-support

 -- Sandro Tosi <morph@debian.org>  Fri, 02 Oct 2009 20:06:05 +0200

python-pyglew (0.1.2-3) unstable; urgency=low

  * debian/control
    - switch Vcs-Browser field to viewsvn
    - updated my email address
    - bump Standards-Version to 3.8.0
      + added debian/README.source
  * debian/rules
    - merged 'rm' calls into 'dh_clean' one
  * debian/copyright
    - updated my email address
    - clarified packaging is licensed under the same terms as upstream code
    - fixed packaging copyright notices, while extending copyright years

 -- Sandro Tosi <morph@debian.org>  Wed, 18 Feb 2009 21:51:52 +0100

python-pyglew (0.1.2-2) unstable; urgency=low

  * debian/control
    - given to DPMT
    - updated build-dep on libglew-dev (Closes: #464935)
    - bump Standard-Version to 3.7.3 (no changes needed)

 -- Sandro Tosi <matrixhasu@gmail.com>  Sun, 10 Feb 2008 15:58:49 +0100

python-pyglew (0.1.2-1) unstable; urgency=low

  * Initial release (Closes: #446790)

 -- Sandro Tosi <matrixhasu@gmail.com>  Wed, 17 Oct 2007 22:51:18 +0200
