
import sys
from pyglew import *

# from pyglut import *
from OpenGL.GLUT import *
from math import *

import pygame
from pygame.locals import *

from PIL import *
import PIL.Image

import bunny2

pygame.display.init()
surface = pygame.display.set_mode((512,512), OPENGL|DOUBLEBUF)

glewInit()
# print glGetError()
print "Vendor =", glGetString(GL_VENDOR)
print "Renderer =", glGetString(GL_RENDERER)
print "Version =", glGetString(GL_VERSION)

#print glGenTextures(1)
#print glGenLists(2)

texId = glGenTextures(1)
glBindTexture(GL_TEXTURE_2D, texId)
im = PIL.Image.open('test.png')
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, im.tostring("raw","RGB",0,-1))
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0)

glEnable(GL_LIGHTING)
glEnable(GL_LIGHT0)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)

glEnableClientState(GL_VERTEX_ARRAY)
glEnableClientState(GL_NORMAL_ARRAY)


t = 0.0
while 1:
    t += 0.1
    event = pygame.event.poll()
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        sys.exit(0)

    glVertexPointer(3, GL_FLOAT, 0, bunny2.vertices)
    glNormalPointer(GL_FLOAT, 0, bunny2.normals)

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, [1,0,0,0])


    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glFrustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 );
    
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glColor3f(1,1,1)
    glTranslatef(0,0,-3)
    #glRotatef(29*cos(t/50), 1, 0, 0)
    #glRotatef(-67*(t/30), 0, 1, 0)
    #glRotatef(30*cos(-t/80), 1, 0, 0)

    #glutSolidTeapot(1.0);

    glDrawArrays(GL_TRIANGLES, 0, len(bunny2.vertices)/3)

    glTranslatef(0,0,-2)

    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, texId)
    # glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE)

    glScalef(2,2,2)
    glBegin(GL_QUADS)
    glTexCoord2f(0,0)
    glVertex3f(-1,-1,0)

    glTexCoord2f(1,0)
    glVertex3f(1,-1,0)

    glTexCoord2f(1,1)
    glVertex3f(1,1,0)

    glTexCoord2f(0,1)
    glVertex3f(-1,1,0)
    glEnd()

    glDisable(GL_TEXTURE_2D)

    w,h = surface.get_size()
    data = glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE)
    im = PIL.Image.fromstring("RGB", (w,h), data)
    im = im.transpose(PIL.Image.FLIP_TOP_BOTTOM)
    im.save('screenshot.png')
    
    pygame.display.flip()
