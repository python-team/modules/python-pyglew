
#include "SDL/SDL.h"
#include "GL/glew.h"
#include <cmath>
#include <cstdlib>


const size_t size = 100;
GLuint pbo = 0;
GLfloat pixels[size*3];


void init_opengl()
{
  glewInit();

  glEnable( GL_TEXTURE_2D );  
  glEnable( GL_LIGHTING );
  glEnable( GL_LIGHT0 );
  glEnable( GL_COLOR_MATERIAL );

  glEnableClientState( GL_VERTEX_ARRAY );

  glGenBuffers(1, &pbo);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_ARB, pbo);
  glBufferData(GL_PIXEL_PACK_BUFFER_ARB, size*3, NULL, GL_DYNAMIC_DRAW);

  for ( size_t i = 0 ; i < 3*size; ) {
    GLfloat theta = i*2*M_PI/(3*size);
    
    pixels[i++] = cos(theta);
    pixels[i++] = sin(theta);
    pixels[i++] = 0.0;
  }

}


void handle_event( const SDL_Event& event )
{
  switch ( event.type ) {
  case SDL_QUIT:
    exit(0);
    break;

  case SDL_KEYDOWN:
    if ( event.key.keysym.sym == SDLK_ESCAPE ) {
      exit(0);
    }
    break;

  default:
    break;
  }
}

void display()
{
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  glDrawPixels(size, 1, GL_RGB, GL_FLOAT, pixels);
  glReadPixels(0, 0, size, 1, GL_RGB, GL_FLOAT, 0);

  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  glOrtho(-1, 1, -1, 1, 0.1, 10.0);

  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity();
  glTranslatef(0,0,-1);

  glBindBuffer(GL_ARRAY_BUFFER, pbo);
  glVertexPointer(3, GL_FLOAT, 0, 0);
  glDrawArrays(GL_POINTS, 0, size);


  SDL_GL_SwapBuffers();
}

int main( int argc, char *argv[] )
{
  SDL_Init( SDL_INIT_VIDEO|SDL_INIT_NOPARACHUTE );
  atexit( SDL_Quit );

  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

  SDL_SetVideoMode( 512, 512, 32, SDL_OPENGL );

  init_opengl();

  while ( true ) {
    SDL_Event event;
    
    while ( SDL_PollEvent( &event ) ) {
      handle_event( event );
    }
    
    display();
  }

}

