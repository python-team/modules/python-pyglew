
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////



inline PyObject* pack(unsigned char v)
{
  return PyInt_FromLong(v);
}

inline PyObject* pack(unsigned int v)
{
  return PyInt_FromLong(v);
}

inline PyObject* pack(int v)
{
  return PyInt_FromLong(v);
}

inline PyObject* pack(const GLubyte * v)
{
  if ( v == 0 ) { 
    Py_INCREF(Py_None);
    return Py_None;
  } else {
    return PyString_FromString(reinterpret_cast<const char*>(v));
  }
}

inline PyObject* pack(const char * v)
{
  return PyString_FromString(v);
}

inline PyObject* pack( GLvoid* ptr )
{
  return PyInt_FromLong(reinterpret_cast<long>(ptr));
}

inline PyObject* pack(GLsizei n, GLushort* is)
{
  if ( n < 0 ) {
    PyErr_SetString( PyExc_ValueError, "Cannot pack tuple of negative size!" );
    return NULL;
  }
  
  switch (n) {
  case 0:
    Py_INCREF(Py_None);
    return Py_None;

  case 1:
    return PyInt_FromLong(is[0]);

  default:
    PyObject* t = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ ) 
      PyTuple_SET_ITEM(t, i, PyInt_FromLong(is[i]));
    return t;
  }
}


inline PyObject* pack(GLsizei n, GLint* is)
{
  if ( n < 0 ) {
    PyErr_SetString( PyExc_ValueError, "Cannot pack tuple of negative size!" );
    return NULL;
  }
  
  switch (n) {
  case 0:
    Py_INCREF(Py_None);
    return Py_None;

  case 1:
    return PyInt_FromLong(is[0]);

  default:
    PyObject* t = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ ) 
      PyTuple_SET_ITEM(t, i, PyInt_FromLong(is[i]));
    return t;
  }
}


inline PyObject* pack(GLsizei n, GLuint* is)
{
  if ( n < 0 ) {
    PyErr_SetString( PyExc_ValueError, "Cannot pack tuple of negative size!" );
    return NULL;
  }
  
  switch (n) {
  case 0:
    Py_INCREF(Py_None);
    return Py_None;

  case 1:
    return PyInt_FromLong(is[0]);

  default:
    PyObject* t = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ ) 
      PyTuple_SET_ITEM(t, i, PyInt_FromLong(is[i]));
    return t;
  }
}

inline PyObject* pack(GLsizei n, GLboolean* is)
{
  if ( n < 0 ) {
    PyErr_SetString( PyExc_ValueError, "Cannot pack tuple of negative size!" );
    return NULL;
  }
  
  switch (n) {
  case 0:
    Py_INCREF(Py_None);
    return Py_None;

  case 1:
    return PyInt_FromLong(is[0]);

  default:
    PyObject* t = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ ) 
      PyTuple_SET_ITEM(t, i, PyInt_FromLong(is[i]));
    return t;
  }
}

inline PyObject* pack(GLsizei n, GLfloat* is)
{
  if ( n < 0 ) {
    PyErr_SetString( PyExc_ValueError, "Cannot pack tuple of negative size!" );
    return NULL;
  }
  
  switch (n) {
  case 0:
    Py_INCREF(Py_None);
    return Py_None;

  case 1:
    return PyFloat_FromDouble(is[0]);

  default:
    PyObject* t = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ ) 
      PyTuple_SET_ITEM(t, i, PyFloat_FromDouble(is[i]));
    return t;
  }
}

inline PyObject* pack(GLsizei n, GLdouble* is)
{
  if ( n < 0 ) {
    PyErr_SetString( PyExc_ValueError, "Cannot pack tuple of negative size!" );
    return NULL;
  }
  
  switch (n) {
  case 0:
    Py_INCREF(Py_None);
    return Py_None;

  case 1:
    return PyFloat_FromDouble(is[0]);

  default:
    PyObject* t = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ ) 
      PyTuple_SET_ITEM(t, i, PyFloat_FromDouble(is[i]));
    return t;
  }
}

inline PyObject* pack(GLsizei n, GLvoid** is)
{
  if ( n < 0 ) {
    PyErr_SetString(PyExc_ValueError, "Negative size!" );
    return NULL;
  }

  if ( n == 1 ) {
    return pack(is[0]);
  } else {
    PyObject* tuple = PyTuple_New(n);
    for ( int i = 0 ; i < n ; i++ )  
      PyTuple_SET_ITEM(tuple, i, pack(is[i]));
    return tuple;
  }
}
