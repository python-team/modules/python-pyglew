
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////

#include "pyglew_exception.hh"
#include "pointer_wrapper.hh"

#ifndef __unpack_ptr_hh__
#define __unpack_ptr_hh__


template<class T>
inline pointer_wrapper<T> unpack_ptr(PyObject* object) throw (pyglew_exception)
{
  assert(false); 
  return pointer_wrapper<T>(0);
}

// template<class T>
// inline void pyglew_cast_exception(PyObject* obj) throw (pyglew_exception)
// {
//   throw pyglew_exception("Don't know yet!");
// }


template<>
inline pointer_wrapper<const GLfloat*> unpack_ptr(PyObject* object) throw (pyglew_exception)
{
  if ( PySequence_Check(object) ) {
    int n = PySequence_Size(object);

    GLfloat* ptr = new GLfloat[n];
    for ( int i = 0 ; i < n ; i++ ) 
      ptr[i] = PyFloat_AsDouble(PySequence_ITEM(object, i));
    return pointer_wrapper<const GLfloat*>(ptr, true);
  } 

  if ( PyObject_CheckReadBuffer(object) ) {
    float* ptr = 0;
    int length = 0;
    
    if ( PyObject_AsReadBuffer(object, const_cast<const void**>((void**)&ptr), &length) < 0 ) {
      throw pyglew_exception("Internal error when trying to convert %s to 'const GLfloat*': PyObject_AsReadBuffer failed!", 
                             PyString_AsString(PyObject_Str(PyObject_Type(object))));

    }
    
    return pointer_wrapper<const GLfloat*>(ptr, false);    
  }

  throw pyglew_exception("Trying to cast %s to 'const GLfloat*'", 
                         PyString_AsString(PyObject_Str(PyObject_Type(object))));
  return 0;
}

template<>
inline pointer_wrapper<const GLuint*> unpack_ptr(PyObject* object) throw (pyglew_exception)
{
  if ( PySequence_Check(object) ) {
    int n = PySequence_Size(object);

    GLuint* ptr = new GLuint[n];
    for ( int i = 0 ; i < n ; i++ ) 
      ptr[i] = PyLong_AsUnsignedLong(PySequence_ITEM(object, i));
    return pointer_wrapper<const GLuint*>(ptr, true);
  } 

  if ( PyObject_CheckReadBuffer(object) ) {
    GLuint* ptr = 0;
    int length = 0;
    
    if ( PyObject_AsReadBuffer(object, const_cast<const void**>((void**)&ptr), &length) < 0 ) {
      throw pyglew_exception("Internal error when trying to convert %s to 'const GLfloat*': PyObject_AsReadBuffer failed!", 
                             PyString_AsString(PyObject_Str(PyObject_Type(object))));

    }
    
    return pointer_wrapper<const GLuint*>(ptr, false);    
  }

  throw pyglew_exception("Trying to cast %s to 'const GLuint*'", 
                         PyString_AsString(PyObject_Str(PyObject_Type(object))));
  return 0;
}


template<>
inline pointer_wrapper<const GLvoid*> unpack_ptr(PyObject* object)
{
  if ( object == Py_None )
    return pointer_wrapper<const GLvoid*>(0);

  if ( PyObject_CheckReadBuffer(object) ) {
    float* ptr = 0;
    int length = 0;
    
    if ( PyObject_AsReadBuffer(object, const_cast<const void**>((void**)&ptr), &length) < 0 ) {
      throw pyglew_exception("Internal error when trying to convert %s to 'const GLfloat*': PyObject_AsReadBuffer failed!", 
                             PyString_AsString(PyObject_Str(PyObject_Type(object))));

    }
    
    return pointer_wrapper<const GLvoid*>(ptr);    
  }

  throw pyglew_exception("Trying to cast '%s' to 'const GLvoid*'", 
                         PyString_AsString(PyObject_Str(PyObject_Type(object))));
  return 0;
}



template<class T>
pointer_wrapper<GLvoid*> array_unpack( GLsizei& stride, PyObject* obj )
{
//   if ( stride == 0 )
//     stride = 1;

  if ( PyList_Check(obj) ) {
    size_t objLen = PyList_Size(obj)/stride;
    T* ptr = reinterpret_cast<T*>( malloc(objLen*sizeof(T)) );
    
    for ( size_t i = 0 ; i < objLen ; i += stride ) {
      ptr[i] = unpack<T>(PyList_GET_ITEM(obj, i));
    }

    stride = 0;
    return pointer_wrapper<GLvoid*>(ptr);

  } else if ( PyTuple_Check(obj) ) {
    size_t objLen = PyTuple_Size(obj)/stride;
    T* ptr = reinterpret_cast<T*>( malloc(objLen*sizeof(T)) );
    
    for ( size_t i = 0 ; i < objLen ; i += stride ) {
      ptr[i] = unpack<T>(PyTuple_GET_ITEM(obj, i));
    }

    stride = 0;
    return pointer_wrapper<GLvoid*>(ptr);

  } else if ( PyInt_Check(obj) ) {
    // "Raw" pointer. Mostly used to reset a pointer or when using 
    // vertex/pixel buffer objects 
    long v = PyInt_AsLong(obj);
    return pointer_wrapper<GLvoid*>(reinterpret_cast<void*>(v));

  } else if ( PyString_Check(obj) ) {
    // "Raw" data string. 
    char *ptr = PyString_AsString(obj);
    return pointer_wrapper<GLvoid*>(reinterpret_cast<void*>(ptr));

  } else {
    throw pyglew_exception("array_unpack: %s", PyString_AsString(PyObject_Str(PyObject_Type(obj))));
  }
}



inline pointer_wrapper<GLvoid*> unpack_pointer( GLenum type, GLsizei& stride, PyObject* obj )
{
  switch ( type ) {
  case GL_BOOL:
    return array_unpack<GLboolean>(stride, obj);

  case GL_SHORT:
  case GL_INT:
    return array_unpack<GLint>(stride, obj);
    
  case GL_FLOAT:
  case GL_DOUBLE:
    return array_unpack<GLfloat>(stride, obj);

  default:
    throw pyglew_exception("unpack_pointer: unknown type 0x%x", type);
  }
}





#endif /* __unpack_ptr_hh__ */
