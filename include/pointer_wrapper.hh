
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////


#ifndef __pointer_wrapper_hh__
#define __pointer_wrapper_hh__

template<class T>
class pointer_wrapper {
public:
  pointer_wrapper( T ptr, bool owns = false ) : _ptr(ptr), _owns(owns) {}
  pointer_wrapper( const pointer_wrapper<T>& wrapper ) 
    : _ptr(wrapper._ptr), _owns(false) {}
  ~pointer_wrapper() { if ( _owns ) delete _ptr; }

  operator T() { return _ptr; }
  
private:
  T _ptr;
  mutable bool _owns;
};

template <>
class pointer_wrapper<const GLvoid*> {
public:
  pointer_wrapper( const GLvoid* ptr, bool owns = false ) : _ptr(ptr), _owns(owns) {}
  ~pointer_wrapper() { if ( _owns ) free(const_cast<void*>(_ptr)); }

  operator const GLvoid*() { return _ptr; }
  
private:
  const GLvoid* _ptr;
  bool _owns;
};

template <>
class pointer_wrapper<GLvoid*> {
public:
  pointer_wrapper( GLvoid* ptr, bool owns = false ) : _ptr(ptr), _owns(owns) {}
  ~pointer_wrapper() { if ( _owns ) free(_ptr); }

  operator GLvoid*() { return _ptr; }
  
private:
  GLvoid* _ptr;
  bool _owns;
};

// template <>
// pointer_wrapper<GLvoid*>::~pointer_wrapper<GLvoid*>()
// {
//   if ( _owns ) free(_ptr);
// }

// template <>
// pointer_wrapper<const GLvoid*>::~pointer_wrapper<const GLvoid*>()
// {
//   if ( _owns ) free(const_cast<void*>(_ptr));
// }


#endif /* __pointer_wrapper_hh__ */
