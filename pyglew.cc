
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////


#include "Python.h"
#include "GL/glew.h"


#include "unpack.hh"
#include "pack.hh"

#include "pyglew-gen.hh"

PyObject* __glewInit( PyObject*, PyObject* )
{
  glewInit();
  Py_INCREF(Py_None);
  return Py_None;
}

static PyMethodDef pyglew_methods[] = {
  {"glewInit", __glewInit, METH_VARARGS, "Initialize GLEW!" },
#include "pyglew-methods.cc"
  {NULL, NULL, 0, NULL}
}; 

struct PyGLEWConstant {
  int type;
  const char* name;
  int value;
};

static PyGLEWConstant pyglew_constants[] = {
#include "pyglew-constants.cc"
  {0, NULL, 0}
};


PyMODINIT_FUNC
initpyglew()
{
  PyObject* m = Py_InitModule("pyglew", pyglew_methods);
  PyObject* d = PyModule_GetDict(m);
  
  int i = 0;
  while ( pyglew_constants[i].name ) {
    PyObject* obj = PyInt_FromLong(pyglew_constants[i].value);
    PyDict_SetItemString( d, pyglew_constants[i].name, obj);
    Py_DECREF(obj);  
    i++;
  }
}
